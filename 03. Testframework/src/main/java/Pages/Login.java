package Pages;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login extends UserActions {



    private Boolean isLogged = null;
    private Boolean isLoggedOut = null;


    public void login(){
        String username = Utils.getConfigPropertyByKey("userForLogin");
        String password = Utils.getConfigPropertyByKey("password");
        waitForElementVisible("usernameField", 20);
        typeValueInField(username,"usernameField" );
        typeValueInField(password, "passwordField");
        clickElement("loginButton");
        waitForElementVisible("logoutButton", 20);
        verifyOnLoginPage();

    }

    public void loginAsFriend(){
        String username = Utils.getConfigPropertyByKey("friendUsername");
        String password = Utils.getConfigPropertyByKey("password");
        waitForElementVisible("usernameField", 20);
        typeValueInField(username,"usernameField" );
        typeValueInField(password, "passwordField");
        clickElement("loginButton");
        waitForElementVisible("logoutButton", 20);
        verifyOnLoginPage();

    }

    public void logOut(){
        waitForElementVisible("logoutButton",10);
        clickElement("logoutButton");
    }

    public void logInOutDependingOnStatus(){
        try{
            Thread.sleep(1500);
            WebElement element = createElement("loginButton");
            element.isDisplayed();
            login();
        }catch (NoSuchElementException e){
            System.out.println("In before catch");
            logOut();
            System.out.println("In after catch");
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }

    public void logInOutDependingOnStatusFriend(){
        try{
            WebElement element = createElement("loginButton");
            element.isDisplayed();
            login();
        }catch (NoSuchElementException e){
            System.out.println("In before catch");
            logOut();
            System.out.println("In after catch");
        }
    }



    public void assertConfirmationMessage(String expText, String location){
        String alertSuccessRegistration = Utils.getUIMappingByKey(location);
        System.out.println(location + " " + alertSuccessRegistration);
        waitForElementVisible(alertSuccessRegistration,20);
        assertTextEquals(Utils.getConfigPropertyByKey(expText), alertSuccessRegistration);
    }

    public void verifyOnLoginPage(){
        isLogged = waitElementVisible("logoutButton",20);
        if(isLogged){
            Utils.LOG.info("User is successfully Logged In");
        }
        else {
            Utils.LOG.info("User is could not be Logged In");
        }
    }

    public void verifyUserLoggedOut(){
        isLoggedOut = waitElementVisible("loginButton",20);
        if(isLoggedOut){
            Utils.LOG.info("User is successfully Logged Out");
        }
        else {
            Utils.LOG.info("User is could not be Logged Out");
        }
        assertElementPresent("loginButton");
    }






}