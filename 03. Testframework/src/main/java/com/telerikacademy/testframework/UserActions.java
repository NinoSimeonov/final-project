package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UserActions {
	final WebDriver driver;


	public UserActions() {
		this.driver = Utils.getWebDriver();
	}

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void editPage() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("editPage.url"));
	}


	public static void sendRequestPage() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("sendRequestPage.url"));
	}

	public static void loadLandingPage() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	public WebElement createElement(String path){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(path)));
		return element;
	}
	// #############   REGISTRATION METHODS   #############


//	public void assertConfirmationMessageDisplayed(String expText){
//		String alertSuccessRegistration = Utils.getUIMappingByKey("alertSuccessRegistration");
//		waitForElementVisible(alertSuccessRegistration,10);
//		assertTextEquals(Utils.getConfigPropertyByKey(expText), alertSuccessRegistration);
//
//	}



	// #############   CLICK   #############


	public void clickElement(String key){
		Utils.LOG.info("Clicking on element " + key);
		waitForElementVisible(key, 10);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
	}


    public void clickRecentPostButton(String key){
		boolean button =  verifyElementPresent(key);
		if(button){

		}else{
			waitElementVisible("recentPostsOfYourFriendButton",10);
			clickElementJS("recentPostsOfYourFriendButton");
		}
    }

	public void clickElementJS (String locator){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Utils.LOG.info("Clicking on element " + locator);
	}


	public void fileUplood (String text, String locator){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, text);
		Utils.LOG.info("Typing comment: " + text);
	}


	// #############   TYPE   #############


	public void typeValueInField(String value, String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(value);
	}

	public void hitENTER(String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(Keys.ENTER);
	}

	public void clearFieldText(String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(Keys.CONTROL+"a");
		element.sendKeys(Keys.BACK_SPACE);
	}

	public void typeValueInFieldByConfProperty(String value, String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(Utils.getConfigPropertyByKey(value));
	}


	public String typeRandomValueInField(int length, String field){
		String randomText = getRandomString(length);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(randomText);
		return randomText;
	}

	public void typeTextJS (String text, String locator) {
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, text);
		Utils.LOG.info("Typing comment: " + text);
	}



	//Add method to clear field before typing in it
	//Method is implemented in typeValueInField(String value, String field)
	public void clearField(String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.click();
		element.sendKeys(Keys.CONTROL+"a");
		element.sendKeys(Keys.BACK_SPACE);
	}

	public void clearFieldForMacOS(String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(Keys.COMMAND+"a");
		element.sendKeys(Keys.BACK_SPACE);
	}

	public String getRandomString(int n) {

		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "0123456789"
				+ "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index
					= (int)(AlphaNumericString.length()
					* Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString
					.charAt(index));
		}

		return sb.toString();
	}


	// #############   GET   #############

	public void getPageTitle(String url){
		String title = driver.getTitle();

		if(title.equals(Utils.getConfigPropertyByKey(url))){
			Utils.LOG.info("Page Title: " + title + " User is on Expected Page!");
		}
		else {
			Utils.LOG.info("Page Title: " + title + " User is not on Expected Page!");
		}
	}

	public String getText(String locator){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		return element.getText();
	}

	//############# WAITS #########

	public void waitForElementVisible(String locator, int seconds){
		WebDriverWait wait = new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void waitForElementVisibleLink(String locator, int seconds){
		WebDriverWait wait = new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(Utils.getUIMappingByKey(locator))));
	}

	public Boolean waitElementVisible(String locator, int seconds){
		WebDriverWait wait = new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		if (element.isDisplayed()) {
			return true;
		}
		else {
			return false;
		}
	}

	public void waitForElementClickable(String locator, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
	}
	//############# ASSERTS #########

	public void assertOnExpectedPage(String url){
		String title = driver.getTitle();
		Utils.LOG.info("User is on " + title + " Page!");
		Assert.assertEquals(Utils.getConfigPropertyByKey(url), title);

	}

	public void assertElementPresent(String locator){
		Utils.LOG.info("Xpath exists: " + locator);
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public Boolean verifyElementPresent(String locator){
		Boolean isElementDisplayed;
		Utils.LOG.info("Xpath exists: " + locator);
		isElementDisplayed = (driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)))).isDisplayed();
		return isElementDisplayed;
	}

	public void assertElementNotPresent(String locator) {
		Utils.LOG.info("Xpath does NOT exist: " + locator);
		Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
	}

	public void assertTextEquals(String expectedText, String locator){
		WebDriverWait wait= new WebDriverWait(driver,10);
		WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
		String textOfWebElement = webElement.getText();
		Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
		Assert.assertEquals(expectedText, textOfWebElement);
	}

	public void assertTextNotEquals(String expectedText, String locator){
		WebDriverWait wait= new WebDriverWait(driver,10);
		WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
		String textOfWebElement = webElement.getText();
		Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
		Assert.assertNotEquals(expectedText, textOfWebElement);
	}

	//############# VERIFY #########

	public void verifyTextEquals(String expectedText, String locator){
		WebDriverWait wait= new WebDriverWait(driver,10);
		WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
		String textOfWebElement = webElement.getText();
		if(expectedText.equals(textOfWebElement)){
			Utils.LOG.info("Expected text: " + expectedText + " is the same as the Actual text!");
		}
		else {
			Utils.LOG.info("Expected text: " + expectedText + " is not the same as the Actual text!");
		}
	}



	//#############CLEAR USER UNFO METOD##################

	public void clearFieldsUser() {
		waitForElementVisible("accountSettingsButton",10);
		clickElementJS("accountSettingsButton");
		waitForElementVisible("editProfileButton",10);
		clickElementJS("editProfileButton");
		waitForElementVisible("posswordFieldEditPage",10);
		clearField("posswordFieldEditPage");
		typeValueInFieldByConfProperty("password","posswordFieldEditPage");
		waitForElementVisible("firstNameFieldEditPage",10);
		clearField("firstNameFieldEditPage");
		waitForElementVisible("lastNameFieldEditPage",10);
		clearField("lastNameFieldEditPage");
		clearField("ageFieldEditPage");
		typeValueInFieldByConfProperty("age","ageFieldEditPage");
		waitForElementVisible("jobTitleFieldEditPage",10);
		clearField("jobTitleFieldEditPage");
		waitForElementVisible("saveEditPageButton",10);
		clickElement("saveEditPageButton");
	}


}
