package stepdefinitions;

import com.telerikacademy.testframework.Registration;
import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.*;

public class RegisterStepDefinitions extends BaseStepDefinitions {

    UserActions actions = new UserActions();
    Registration registration = new Registration();


    @Then("$element Message is Displayed at $location")
    public void assertMessageDisplayed(String element, String location){
        registration.assertConfirmationMessage(element, location);
    }


    @Then("User remains on $pageTitle Page")
    public void userNotRegisteredWithoutUsername(String pageTitle){
        actions.waitForElementVisible("registrationFormBody",10);
        actions.assertOnExpectedPage(pageTitle);
    }



}
