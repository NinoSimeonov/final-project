package stepdefinitions;

import Pages.Login;
import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class LoginStepDefinitions extends BaseStepDefinitions {

    UserActions actions = new UserActions();
    Login login = new Login();


    @Given("User enters the Social Network successfully")
    public void loginAsFriend(){
        login.logInOutDependingOnStatusFriend();

    }

    @Given("User login to the Social Network successfully")
    public void logIn(){
        login.logOut();
        login.login();

    }

    @Then("$element Message is Displayed at $location")
    public void assertMessageDisplayed(String element, String location) {
        login.assertConfirmationMessage(element, location);
    }

    @Then("User remains on $pageTitle Page")
    public void userNotRegisteredWithoutUsername(String pageTitle) {
        actions.waitForElementVisible("registrationFormBody", 15);
        actions.assertOnExpectedPage(pageTitle);
    }

    @Given("User is logged In")
    public void userIsLoggedIn(){
        login.verifyOnLoginPage();
    }

    @When("User is successfully logged Out")
    @Then("User is successfully logged Out")
    public void userIsLoggedOut(){
        login.verifyUserLoggedOut();
    }


    @Given("User enters Social Network Application")
    @When("User enters Social Network Application")
    @Then("User enters Social Network Application")
    public void login(){
        login.logInOutDependingOnStatus();

    }


    @Given("User leaves Social Network Application")
    @When("User leaves Social Network Application")
    @Then("User leaves Social Network Application")
    public void logOut(){
        login.logOut();

    }
}



