package stepdefinitions;

import com.telerikacademy.testframework.Registration;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();
    Registration registration = new Registration();


//    ---------------------------------- TYPE ----------------------------------

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    @Aliases(values={
            "User enters $value in username $field",
            "User enters $value in email $field",
            "User enters $value in password $field",
            "User enters $value in confirm password $field",
            "User enters $randomText in $newPostTextarea"})
    public void typeInField(String value, String field){
        actions.waitForElementVisible(field, 15);
        actions.clearField(field);
        actions.typeValueInFieldByConfProperty(value, field);
    }

    @When ("User enters randomText in $newPostTextarea")
    public void typeRandomText(String field){
        actions.waitForElementVisible(field,15);
        actions.clearField(field);
        actions.typeRandomValueInField(30, field);

    }



//    ---------------------------------- PAGE VERIFICATION ----------------------------------

    @Given("User is redirected to $pageTitle Page")
    @Then("User is redirected to $pageTitle Page")
    public void verifyPageTitle(String pageTitle){
        actions.getPageTitle(pageTitle);
        try{
            Thread.sleep(1000);
        }catch(InterruptedException e){
            System.out.println(e.getMessage());
        }
    }




//    ---------------------------------- CLICK ----------------------------------
    @When("User clicks on $element Link")
    @Then("User clicks on $element Link")
    @Alias("User clicks on $element Button")
    public void clickElement(String element){

        actions.waitForElementVisible(element, 20);
        actions.clickElement(element);
    }





}
