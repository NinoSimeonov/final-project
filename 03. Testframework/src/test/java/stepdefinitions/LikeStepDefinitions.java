package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class LikeStepDefinitions extends BaseStepDefinitions {

    UserActions actions = new UserActions();

    String likes;
    int beginNumLikes;
    int endNumLikes;
    boolean isLikeCounterIncreased;
    boolean isLikeCounterDecreased;

    public void enterFriendProfile(){
        actions.waitForElementVisible("viewJoe2Button",10);
        actions.clickElement("viewJoe2Button");
    }

    public int getPostNumLikes(){
        actions.waitForElementVisible("numLikes",10);
        likes = actions.getText("numLikes");
        System.out.println(likes);
        String[] numLikes = likes.split(" ");
        System.out.println(numLikes[0]);
        int numberOfLikes = Integer.parseInt(numLikes[0]);
        System.out.println(numberOfLikes);
        return numberOfLikes;

    }

    public void likePost(){
        actions.waitForElementVisible("topPostLikeButton",10);
        actions.clickElement("topPostLikeButton");
    }

    @When("User likes Friend Post")
    public void likeFriendPost(){
        enterFriendProfile();
        beginNumLikes = getPostNumLikes();
        likePost();
        endNumLikes = getPostNumLikes();
        isLikeCounterIncreased = false;

    }

    @When("User unlikes Friend Post")
    public void unlikeFriendPost(){
        enterFriendProfile();
        beginNumLikes = getPostNumLikes();
        try{Thread.sleep(2000);}catch (InterruptedException ed){System.out.println(ed.getMessage());}
        likePost();
        try{Thread.sleep(2000);}catch (InterruptedException e){System.out.println(e.getMessage());}
        endNumLikes = getPostNumLikes();
        isLikeCounterDecreased = false;

    }


    @Then("Post likeCounter is increased")
    public void confirmLikeCounterIncreased(){
        endNumLikes = getPostNumLikes();
        if(endNumLikes>beginNumLikes){
            isLikeCounterIncreased = true;
        }
//        Assert.assertEquals(endNumLikes,beginNumLikes+1);
       Assert.assertTrue(isLikeCounterIncreased);
    }

    @Then("Post likeCounter is decreased")
    public void confirmLikeCounterDecreased(){
        if(endNumLikes<beginNumLikes){
            isLikeCounterDecreased = true;
        }
//        Assert.assertEquals(beginNumLikes,endNumLikes+1);
        Assert.assertTrue(isLikeCounterDecreased);
    }

}
