Feature: Login new User to the Social Network

Meta:
@login

Narrative:
As a registered user
I want to login into Social Network
So that I can use different functionalities



Scenario: Successful Login with Correct Credentials
Given User is redirected to landingPage Page
When User enters Social Network Application
Then User is redirected to homePage Page
And User clicks on logoutButton Button


Scenario: Successful Logout
Given User enters Social Network Application
Given User is redirected to homePage Page
When User clicks on logoutButton Button
Then User is redirected to homePage Page


Scenario: Try to create successful Login without username provided
Given User is redirected to landingPage Page
When User enters password in password passwordField
And User clicks on loginButton Button
Then unsuccessLoginMessage Message is Displayed at loginMessage


Scenario: Try to create successful Login without password provided
Given User is redirected to landingPage Page
When User enters userForLogin in username usernameField
And User clicks on loginButton Button
Then unsuccessLoginMessage Message is Displayed at loginMessage


Scenario: Try to create successful Login without username and password provided
Given User is redirected to landingPage Page
When User clicks on loginButton Button
Then unsuccessLoginMessage Message is Displayed at loginMessage


Scenario: Try to create successful Login with valid username and invalid password provided
Given User is redirected to landingPage Page
When User enters userForLogin in username usernameField
And User enters wrongPassword in password passwordField
And User clicks on loginButton Button
Then unsuccessLoginMessage Message is Displayed at loginMessage


Scenario: Try to create successful Login with invalid username and valid password provided
Given User is redirected to landingPage Page
When User enters username in username usernameField
And User enters password in password passwordField
And User clicks on loginButton Button
Then unsuccessLoginMessage Message is Displayed at loginMessage


Scenario: Try to create successful Login with invalid username and invalid password provided
Given User is redirected to landingPage Page
When User enters username in username usernameField
And User enters wrongPassword in password passwordField
And User clicks on loginButton Button
Then unsuccessLoginMessage Message is Displayed at loginMessage






