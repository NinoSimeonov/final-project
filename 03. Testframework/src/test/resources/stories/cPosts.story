Feature: User is using the Post features

Meta:
@posts

Narrative:
As a user
I want to use the Post Features
So that I create, edit and delete Posts

Scenario: Create new Public Post successfully as an existing User
Given User enters Social Network Application
When User clicks on newsIcon Link
Then User is redirected to newsFeed Page
When User enters newPostBodyText in newPostTextarea
And User clicks on StatusIcon Button
And User clicks on publicStatusIcon Button
And User clicks on sharePost Button
Then User leaves Social Network Application
And User clicks on publicPostsButton Button
And User clicks on publicPostsLink Button
Then Post with newPostBodyText is listed as newestPost



Scenario: Create new Private Post successfully as an existing User
Given User enters Social Network Application
When User clicks on newsIcon Link
Then User is redirected to newsFeed Page
When User enters newPostBodyTextPvt in newPostTextarea
And User clicks on StatusIcon Button
And User clicks on privateStatusIcon Button
And User clicks on sharePost Button
Then Post with newPostBodyTextPvt is listed as newestPost on Post Feed
Then User leaves Social Network Application
And User clicks on publicPostsButton Button
And User clicks on publicPostsLink Button
Then Post with newPostBodyTextPvt is not listed as newestPost



Scenario: Edit own Post Text
Given User enters Social Network Application
When User clicks on newsIcon Link
Then User is redirected to newsFeed Page
And User clicks on modifyPostMenuButton Button
And User clicks on editPostButton Button
Then User is redirected to editPage Page
And User enters editPostBodyText in editPostTextarea
And User clicks on editPostSubmitButton Button
Then Post with editPostBodyText is listed as newestPost
Then User leaves Social Network Application



Scenario: Edit own Post Status
Given User enters Social Network Application
When User clicks on newsIcon Link
Then User is redirected to newsFeed Page
And User clicks on modifyPostMenuButton Button
And User clicks on editPostButton Button
Then User is redirected to editPage Page
And User clicks on editPostStatusDropdown Button
And User clicks on editPostStatusPublic Button
And User clicks on editPostSubmitButton Button
Then User leaves Social Network Application
And User clicks on publicPostsButton Button
And User clicks on publicPostsLink Button
Then Post with editPostBodyText is listed as newestPost
Then User returns to Landing Page
And User clicks on logoutButton Button

Scenario: Delete Own Post
Given User enters Social Network Application
When User clicks on newsIcon Link
Then User is redirected to newsFeed Page
And User clicks on modifyPostMenuButton Button
And User clicks on deletePostButton Button
And User clicks on yesConfirmDeleteButton Button
And User clicks on okDeleteButton Button
Then Post with editPostBodyText isn't listed as newestPost at the top of the Post Feed
Then User leaves Social Network Application
