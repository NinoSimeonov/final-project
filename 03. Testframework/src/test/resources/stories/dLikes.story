Feature: User is using the Like features

Meta:
@likes

Narrative:
As a user
I want to access friends Posts
So that I can like and unlike them

Scenario: User Likes Friends Post
Given User enters Social Network Application
When User likes Friend Post
Then Post likeCounter is increased
Then User leaves Social Network Application

Scenario: User Unlikes Friends Post
Given User enters Social Network Application
When User unlikes Friend Post
Then Post likeCounter is decreased
Then User leaves Social Network Application
