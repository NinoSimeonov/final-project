Feature: Registering new User to the Social Network

Meta:
@registration

Narrative:
As a user
I want to fill registration form
So that I can become legitimate User of the Social Network


Scenario: Try create successful registration without username provided
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters email in email emailField
And User enters password in password passwordField
And User enters password in confirm password confirmPassField
When User clicks on checkbox.TCs Button
And User clicks on submitRegistration Button
Then User remains on signupPage Page


Scenario: Try create successful registration without email provided
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters username in username usernameField
And User enters password in password passwordField
And User enters password in confirm password confirmPassField
When User clicks on checkbox.TCs Button
And User clicks on submitRegistration Button
Then User remains on signupPage Page



Scenario: Try Create successful Registration without Password provided
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters username in username usernameField
And User enters email in email emailField
And User enters password in confirm password confirmPassField
When User clicks on checkbox.TCs Button
And User clicks on submitRegistration Button
Then User remains on signupPage Page


Scenario: Try Create successful Registration by providing Wrong Confirmation Password
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters username in username usernameField
And User enters email in email emailField
And User enters password in password passwordField
And User enters wrongPassword in confirm password confirmPassField
When User clicks on checkbox.TCs Button
And User clicks on submitRegistration Button
Then User remains on signupPage Page


Scenario: Try Create successful Registration without agreeing on Terms and Conditions
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters username in username usernameField
And User enters email in email emailField
And User enters password in password passwordField
And User enters password in confirm password confirmPassField
When User clicks on submitRegistration Button
Then User remains on signupPage Page


Scenario: Create Successful Registration
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters username in username usernameField
And User enters email in email emailField
And User enters password in password passwordField
And User enters password in confirm password confirmPassField
When User clicks on checkbox.TCs Button
And User clicks on submitRegistration Button
Then User is redirected to landingPage Page
Then successConfirmation Message is Displayed at loginForm


Scenario: Try Create successful Registration by providing exising Username
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters existingUsername in username usernameField
And User enters secondEmail in email emailField
And User enters password in password passwordField
And User enters wrongPassword in confirm password confirmPassField
When User clicks on checkbox.TCs Button
And User clicks on submitRegistration Button
Then existUsernameMessage Message is Displayed at registrationForm


Scenario: Try Create successful Registration by providing exising Email
Given User is redirected to landingPage Page
When User clicks on registerButton Link
Then User is redirected to signupPage Page
And User enters secondUsername in username usernameField
And User enters existingEmail in email emailField
And User enters password in password passwordField
And User enters password in confirm password confirmPassField
When User clicks on checkbox.TCs Button
And User clicks on submitRegistration Button
Then existEmailMessage Message is Displayed at registrationForm