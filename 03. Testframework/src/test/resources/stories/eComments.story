Feature: User is using the Comment features

Meta:
@comments

Narrative:
As a user
I want to use the Comment Features
So that I create, edit and delete Posts

Scenario: User Comments Successfully a Post
Given User login to the Social Network successfully
When User comments Friend Post
Then The Comments is Displayed below the Post


Scenario: User Successfully Edited a Comment
Given User login to the Social Network successfully
When User edits a Comment
Then Comment with commentEditText is listed as newestComment


Scenario: User Successfully Deletes a Comment
Given User login to the Social Network successfully
When User deletes Comment
Then Comment is deleted

