# Final Project QA Telerik Academy - Social Network

### 1. Trello link: https://trello.com/b/xjaTUmOP/testcasesforsoclianetwork

### 2. GitLab link: https://gitlab.com/NinoSimeonov/final-project.git

### 3. TestRail link: https://howtoqa.testrail.io/index.php?/projects/overview/1

### 4. DEV Project link: https://gitlab.com/RosenAn/telerik-academy-final-project---social-network.git

### 5. Presentation link : https://www.canva.com/design/DADseLExrBQ/7B6IzujLG54r1KYola2xpQ/edit?fbclid=IwAR0gh1p_Z3p4Rp7N4FPN6SQjxzVG3YyIubENoH5svnsR_casfsa63eEhBzE#1

### 6. Issues link : https://gitlab.com/RosenAn/telerik-academy-final-project---social-network/issues

### 7. Exploratory Testing Report link : https://drive.google.com/open?id=1xvsuTGn4PbM4D2SyjdF5s59CBlEf001S

### 8. Summary Testing Report link : https://drive.google.com/open?id=1cgE1cW1bASuvBZo6vH7YsMK5WTrjMNpE